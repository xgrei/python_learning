"""Exercise 10

Exercise from
https://www.practicepython.org/exercise/2014/04/10/10-list-overlap-comprehensions.html
"""

from random import randint


def exercise_main() -> None:
    """Prints identical values of two lists"""
    # Constants
    INT_A_UPPERLIMIT: int = randint(50, 100)
    INT_B_UPPERLIMIT: int = randint(50, 100)
    INT_A_LOWERLIMIT: int = 0
    INT_B_LOWERLIMIT: int = 0
    INT_A_INCREMENT: int = randint(1, 5)
    INT_B_INCREMENT: int = randint(1, 5)
    INT_LIST_A = list(
        range(INT_A_LOWERLIMIT, INT_A_UPPERLIMIT, INT_A_INCREMENT)
    )
    INT_LIST_B = list(
        range(INT_B_LOWERLIMIT, INT_B_UPPERLIMIT, INT_B_INCREMENT)
    )

    c: set[int] = set([item for item in INT_LIST_A if item in INT_LIST_B])

    print("List A is: " + str(INT_LIST_A))
    print("List B is: " + str(INT_LIST_B))
    print("Identical values of LIST A and B: " + str(c))


if __name__ == "__main__":
    exercise_main()
