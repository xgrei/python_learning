"""Excercise 02

This is implemented Excercise 02 from:
https://www.practicepython.org/exercise/2014/02/05/02-odd-or-even.html
"""


def exercise_main() -> None:
    """Function that tells you if number is even or odd"""
    # Input from user
    INT_NUM: int = int(input("Enter a number to divide: "))
    INT_DIVIDER: int = int(input("Enter dividing number: "))

    if INT_DIVIDER == 0:
        quit("User has entered 0 as invalid divider.")

    if INT_NUM % 4 == 0:
        print("Number is a multiple of 4")
    elif INT_NUM % 2 == 0:
        print(INT_NUM, " is an even number")

    if INT_NUM % INT_DIVIDER > 1:
        print("Number does not divide evenly by ", INT_DIVIDER)
    else:
        print("Number divides evenly with: ", INT_DIVIDER)


if __name__ == "__main__":
    exercise_main()
