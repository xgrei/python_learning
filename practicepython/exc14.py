"""Excercise 13

Excercise from https://www.practicepython.org/exercise/2014/05/15/14-list-remove-duplicates.html
"""

import numpy as np

# Constants
LIST_A: list[int] = list(np.random.randint(low=2, high=50, size=30))


def remove_duplicates_from_list_sets(list_input: int):
    """Function removes duplicates from input list using sets

    Args:
        list_input (int): randomly generated list
    """
    # Variables
    list_b: list[int] = list(set(list_input))

    print("List without duplicates(using sets): " + str(list_b))


def remove_duplicates_from_list_loops(list_input: int):
    """unction removes duplicates from input list using loops

    Args:
        list_input (int): randomly generated list
    """
    # Variables
    list_b: list[int] = []

    for item in list_input:
        if item not in list_b:
            list_b.append(item)

    list_b.sort()
    print("List without duplicates(using loops): " + str(list_b))


def exercise_main(list_input: int):
    """Main function code

    Args:
        list_input (int): randomly generated list
    """
    remove_duplicates_from_list_sets(list_input)
    remove_duplicates_from_list_loops(list_input)


if __name__ == "__main__":
    exercise_main(LIST_A)
