"""Excercise 08

Implemented excercise 08 from this webpage:
https://www.practicepython.org/exercise/2014/03/26/08-rock-paper-scissors.html
"""

import os


def validate_input(usr_input: str, validation_type: str = "command"):
    """This function will make sure that user has entered only one of 
    the valid strings

    Args:
        usr_input (str): user input
        validation_type (str, optional): _description_. Defaults to "command".

    Returns:
        _type_: _description_
    """
    # Setting up proper valid inputs based on validation type
    if validation_type == "game":
        LIST_VALID_INPUTS: tuple = ("rock", "paper", "scissors")
    if validation_type == "command":
        LIST_VALID_INPUTS: tuple = ("c", "quit")

    usr_input: str = usr_input.lower()

    if usr_input not in LIST_VALID_INPUTS:
        while usr_input not in LIST_VALID_INPUTS:
            usr_input = input("Wrong input! Please enter valid input: ")
            usr_input = usr_input.lower()
    return usr_input


print("This is a 2 player Rock - Paper - Scissors game")
print("Commands:\n Start - To start a game \n Quit - To end to program")

command: str = input("Enter a command: ")


def exercise_main(user_command: str) -> None:
    """Main rock/paper/scissors game function"""
    while user_command.lower() != "quit":
        if user_command.lower() == "start" or user_command.lower() == "c":
            if user_command.lower() == "c":
                # Clears screen if we are running new game
                os.system("clear")
            print("Starting New Game!")
            print("Please enter either 'scissors', 'rock' or 'paper'.")
            p1_input: str = validate_input(input("Player1: "), "game")
            p2_input: str = validate_input(input("Player2: "), "game")

            if p1_input == p2_input:
                print("It's a tie!")
            elif (
                p1_input == "rock"
                and p2_input == "scissors"
                or p1_input == "scissors"
                and p2_input == "paper"
                or p1_input == "paper"
                and p2_input == "rock"
            ):
                print("Player1 Wins!")
            else:
                print("Player2 Wins!")

        user_command: str = validate_input(
            input("Type 'quit' to exit or 'c' to continue:"), "command"
        )

    quit("Exiting script!")


if __name__ == "__main__":
    exercise_main(command)
