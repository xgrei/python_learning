"""Excercise 13

Excercise from https://www.practicepython.org/exercise/2014/04/30/13-fibonacci.html
"""

# User input Constant
STR_INPUT_TEXT: str = input("Please enter how many fibonnaci numbers to generate: ")


def fibonnaci_gen(user_input: str):
    """Function to generate a list of fibonnaci numbers based on user input

    Args:
        user_input (str): user input on how many fibonnaci numbers to generate

    Returns:
        LIST[INT]: return fibonnaci list
    """
    # User input validation
    if not user_input.isdecimal():
        quit("Input is not a number! Exiting!")

    # Constants
    LIST_FIBONNACI: list[int] = []

    # variables
    inp = int(user_input)
    previous_num1: int = 0
    previous_num2: int = 1
    fibonnaci_number: int = 1
    i: int = 2

    match inp:
        case inp if inp == 0:
            LIST_FIBONNACI = []
        case inp if inp == 1:
            LIST_FIBONNACI = [0]
        case inp if inp >= 2:
            LIST_FIBONNACI = [0, 1]

    while i < inp:  # povodne <=
        fibonnaci_number = LIST_FIBONNACI[previous_num1] + LIST_FIBONNACI[previous_num2]
        LIST_FIBONNACI.append(fibonnaci_number)
        previous_num1 += 1
        previous_num2 += 1
        i += 1
    return LIST_FIBONNACI


def exercise_main(user_input: str):
    """Main function that prints the generated fibonnaci list

    Args:
        user_input (str): user input on how many fibonnaci numbers to generate
    """    
    print(fibonnaci_gen(user_input))


if __name__ == "__main__":
    exercise_main(STR_INPUT_TEXT)
