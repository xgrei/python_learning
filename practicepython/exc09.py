"""Excercise 09

Excercise from
https://www.practicepython.org/exercise/2014/04/02/09-guessing-game-one.html
"""


from pickle import FALSE
import random


def exercise_main() -> None:
    """Main function for guessing game"""
    # Constants
    INT_LOWER_LIMIT: int = 1
    INT_UPPER_LIMIT: int = 9
    STR_GUESSED_BIGER: str = "Guessed number is too high!"
    STR_GUESSED_LOWER: str = "Guessed number is too low!"
    STR_GUESSED_EQUAL: str = "Correct guess!"
    STR_INPUT_TEXT: str = (
        "Please enter guessed number between "
        + str(INT_LOWER_LIMIT)
        + " and "
        + str(INT_UPPER_LIMIT)
        + " (or type 'exit' to quit):"
    )
    STR_ERROR_INPUT: str = "Input out of range or not a number!"
    INT_GENERATED_NUMBER: int = random.randint(
        INT_LOWER_LIMIT, INT_UPPER_LIMIT
    )

    # Variables
    count: int = 0
    inp: str = str(input(STR_INPUT_TEXT))

    while inp != "exit":
        count += 1

        if inp.isdecimal():
            inp = int(inp)
        else:
            # Indicate error input status if not possible to convert to
            # integer number
            inp = False

        if (inp is False) or not (
            (INT_LOWER_LIMIT <= inp) and (inp <= INT_UPPER_LIMIT)
        ):
            print(STR_ERROR_INPUT)
            count -= 1
            inp = input(STR_INPUT_TEXT)
            continue

        # Lets check if we have guessed correctly - then we dont have to
        # compare any further
        if inp == INT_GENERATED_NUMBER:
            print(STR_GUESSED_EQUAL)
            break
        if inp > INT_GENERATED_NUMBER:
            print(STR_GUESSED_BIGER)
        else:
            print(STR_GUESSED_LOWER)

        inp = input(STR_INPUT_TEXT)

    quit("You have made: " + str(count) + " guesses. Program exited!")


if __name__ == "__main__":
    exercise_main()
