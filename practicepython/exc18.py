"""Exercise 18

Exercise from:
https://www.practicepython.org/exercise/2014/07/05/18-cows-and-bulls.html
"""

import random


def generate_random_number():
    i = 0
    INT_A_LOWER_LIMIT: int = 0
    INT_B_UPPER_LIMIT: int = 9
    INT_LIST_RANDOM_NUMBER: list[int] = []
    while i < 4:
        int_random_number = random.randint(
            INT_A_LOWER_LIMIT, INT_B_UPPER_LIMIT
        )
        if int_random_number not in INT_LIST_RANDOM_NUMBER:
            INT_LIST_RANDOM_NUMBER.append(int_random_number)
            i += 1
        else:
            continue

    print(INT_LIST_RANDOM_NUMBER)


# def bulls_and_cows():
def user_input_verification():

    while True:
        INT_LIST_USER_INPUT: list[int] = list(input("Enter 4 digit number: "))
        INT_LIST_USER_INPUT_NODUP: list[int] = list(set(INT_LIST_USER_INPUT))
        INT_LENGHT: int = len(INT_LIST_USER_INPUT)

        if INT_LENGHT in range(0, 5):
            int_list_user_input_sorted = INT_LIST_USER_INPUT.copy()
            int_list_user_input_nodup_sorted = INT_LIST_USER_INPUT_NODUP.copy()
            int_list_user_input_sorted.sort()
            int_list_user_input_nodup_sorted.sort()

            if int_list_user_input_sorted == int_list_user_input_nodup_sorted:
                print("VALID NUMBER")

                return INT_LIST_USER_INPUT
            else:
                print("INVALID NUMBER")
        else:
            print("NUMBER OUT OF RANGE")

        print(INT_LIST_USER_INPUT)


generate_random_number()
user_input_verification()

# bulls_and_cows()


def exercise_main(lenght: int):
    """Main function to execute generate_password()

    Args:
        lenght (int): user input for password lenght
    """


# if __name__ == "__main__":
# bulls_and_cows(INT_USER_INPUT):
