"""Exercise 01

Script is part of exercise 01 from:
https://www.practicepython.org/exercise/2014/01/29/01-character-input.html
"""

# Standard library
from datetime import date


def exercise_main() -> None:
    """Function that tells you when you turn 100 years old"""
    # Inputs
    STR_NAME: str = str(input("Enter your Name: "))
    INT_AGE: int = int(input("Enter your Age: "))
    INT_COPIES: int = int(input("Enter a number Between 1 - 10: "))

    # Constants
    INT_YEAR_TODAY: int = date.today().year
    INT_YEAR_BORNED: int = INT_YEAR_TODAY - INT_AGE
    INT_YEAR_TURN_HUNDRED: int = INT_YEAR_BORNED + 100
    STR_OUTPUT_TEXT: str = (
        STR_NAME
        + " you will turn 100 years old in: "
        + str(INT_YEAR_TURN_HUNDRED)
    )

    print(STR_OUTPUT_TEXT)
    int_counter: int = 0

    if INT_COPIES in range(0, 11):
        while int_counter < INT_COPIES:
            print(STR_OUTPUT_TEXT)
            int_counter += 1
    else:
        print("Out of range")


if __name__ == "__main__":
    exercise_main()
