
import math
import os
import random
import re
import sys


# Napis sem neaky komentar ze co si sa snazil poriesit? Ake bolo zadanie?
# Vzdy je dobre pisat aspon neaku kratku dokumentaciu aby bolo jasne co ma ten kod riesit.
# Je to z toho dovodu aby ked niekto bude citat tvoj kod alebo ho bude chciet aktualizovat aby v priebehu par minut hned pochopil co to ma robit.
# Z tohto kodu nieje celkom jasne co ma byt jeho funkciou.

# TODO: Nainstalovat "Todo Tree" expansion do Visual Studio Code
# TODO: Napisat dokumentaciu (idealne anglicky)

USER_INPUT: int = int(input().strip())

if USER_INPUT in range(0,101):
    if USER_INPUT in range(2,6,2):
        print("Not Weird")
    elif USER_INPUT in range(6,21,2):
        print("Weird")
    elif USER_INPUT in range(20,101,2):
        print("Not Weird")
    else:
        print("Weird")


USER_INPUT = int(input().strip())

# Tu by som pouzil `in range()` namiesto tych rozsahov tak ako to mas v hornom bloku
# Predpokladam ale ze si tak nespravil len kvoli porovnaniu funkcionality?
# FIXME: Delete
if USER_INPUT >= 0 and USER_INPUT <= 100:
    if USER_INPUT >= 2 and USER_INPUT <=5 and USER_INPUT % 2 == 0:
        print("Not Weird")
    elif USER_INPUT >= 6 and USER_INPUT <= 20 and USER_INPUT % 2 == 0:
        print("Weird")
    elif USER_INPUT >= 20 and USER_INPUT % 2 == 0:
        print("Not Weird")
    else:
        print("Weird")