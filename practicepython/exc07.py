"""Exercise 07

Implemented exercise 07 from this webpage:
https://www.practicepython.org/exercise/2014/03/19/07-list-comprehensions.html
"""


def exercise_main() -> None:
    """returns a list with only even elements"""
    INT_LIST_A: list[int] = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
    LIST_A_EVEN: list[int] = [num for num in INT_LIST_A if num % 2 == 0]

    print(LIST_A_EVEN)


if __name__ == "__main__":
    exercise_main()
