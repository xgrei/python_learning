"""Exercise 04

This script is the implementation of Exercise 04 from:
https://www.practicepython.org/exercise/2014/02/26/04-divisors.html
"""


def exercise_main() -> None:
    """Prints a list of divisors for a number"""
    # Input
    STR_INPUT: str = str(input("Enter a Number to find all divisors: "))

    # User input validation
    if not STR_INPUT.isdecimal():
        quit("Input is not a number! Exiting!")

    if STR_INPUT == "0":
        quit("0 has 0 divisors")

    # Constants
    INT_NUMBER: int = int(STR_INPUT)
    RANGE_NUMBERPOOL: range = range(1, INT_NUMBER + 1)

    LIST_DIVISORS: list = [
        number for number in RANGE_NUMBERPOOL if not INT_NUMBER % number
    ]
    print(
        "Number "
        + str(INT_NUMBER)
        + " have these divisors: "
        + str(LIST_DIVISORS)
    )
    quit("Program exit!")


if __name__ == "__main__":
    exercise_main()
