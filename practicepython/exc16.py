"""Exercise 16

Exercise from https://www.practicepython.org/exercise/2014/05/28/16-password-generator.html
"""

import string
import random

# Constants
INT_PWD_LENGHT: int = int(input("Enter password lenght: "))


def generate_password(lenght: int):
    """Function to generate password, lenght is based on user input

    Args:
        lenght (int): user input for password lenght
    """
    # Constants
    STR_TUPLE_POOL: tuple(str) = string.printable

    # Variables
    password: list[str] = []

    str_list_pool_split: list = [char for char in STR_TUPLE_POOL[0:-6]]
    random.shuffle(str_list_pool_split)

    while 0 < lenght:
        password.append(random.choice(str_list_pool_split))
        lenght -= 1

    print("".join(password))


def exercise_main(lenght: int):
    """Main function to execute generate_password()

    Args:
        lenght (int): user input for password lenght
    """
    generate_password(lenght)


if __name__ == "__main__":
    generate_password(INT_PWD_LENGHT)
