"""Exercise 12

Exercise from https://www.practicepython.org/exercise/2014/04/25/12-list-ends.html
"""


from random import randint


def first_last_element():
    """Function prints first and last item in a random generated list"""
    # Constants
    INT_A_UPPERLIMIT: int = randint(50, 100)
    INT_A_LOWERLIMIT: int = randint(0, 20)
    INT_A_STEP: int = randint(1, 10)
    LIST_A: list[int] = list(range(INT_A_LOWERLIMIT, INT_A_UPPERLIMIT, INT_A_STEP))
    LIST_B: list[int] = []
    LIST_B.append(LIST_A[0])
    LIST_B.append(LIST_A[-1])

    print("Generated list is: " + str(LIST_A))
    print("First and last item of LIST_A is: " + str(LIST_B))


def exercise_main():
    """FUnction to run fist_last_element()"""
    first_last_element()


if __name__ == "__main__":
    exercise_main()
