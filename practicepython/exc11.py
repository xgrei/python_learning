"""Exercise 11

Exercise from https://www.practicepython.org/exercise/2014/04/16/11-check-primality-functions.html
"""

# User Input Constant
STR_INPUT: str = input("Enter a Number to check if it's prime: ")
STR_NUMBER_IS_PRIME: str = "The Number is prime!"
STR_NUMBER_IS_NOT_PRIME: str = "The Number is not prime!"

def is_number_prime(string_input: str) -> bool:
    """Function to check if number is prime or not

    Args:
        string_input (str): user input number to chech if its prime
    """
    # User input validation
    if not string_input.isdecimal():
        quit("Input is not a number! Exiting!")

    # Constants
    INT_NUMBER: int = int(string_input)
    RANGE_NUMBERPOOL: range = range(1, INT_NUMBER + 1)
    

    for number in RANGE_NUMBERPOOL:
        if (
            (INT_NUMBER / number == 1)
            and (INT_NUMBER == number)
            and (INT_NUMBER % number != 0)
            or (INT_NUMBER == 1)
        ):
            return False 
        else:
            return True
            


def exercise_main(string_input: str):
    """Main function which prints if number is prime or not

    Args:
        string_input (str): user input number to chech if its prime
    """           
    if is_number_prime(string_input): print(STR_NUMBER_IS_PRIME)
    else:  print(STR_NUMBER_IS_NOT_PRIME)


if __name__ == "__main__":
    exercise_main(STR_INPUT)
