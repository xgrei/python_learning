"""Exercise 03

This code is an implementation of Exercise 03 from:
https://www.practicepython.org/exercise/2014/02/15/03-list-less-than-ten.html
"""


def exercise_main() -> None:
    """Function prints elements of a list that are less then user input"""
    # Constants
    INT_TUPLE_A: tuple[int] = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    INT_TUPLE_A_MIN: int = min(INT_TUPLE_A)
    INT_TUPLE_A_MAX: int = max(INT_TUPLE_A)
    LIST_LESSTHANFIVE: list = [
        element for element in INT_TUPLE_A if element < 5
    ]

    print("Original list: " + str(INT_TUPLE_A))
    print(
        "Elements from the original list that are less than five: "
        + str(LIST_LESSTHANFIVE)
    )

    STR_INPUT: str = input(
        "Enter a number (from "
        + str(INT_TUPLE_A_MIN)
        + " to "
        + str(INT_TUPLE_A_MAX)
        + ") to select elements that are smaller: "
    )
    if not STR_INPUT.isdecimal() or not (
        INT_TUPLE_A_MIN <= int(STR_INPUT) and int(STR_INPUT) <= INT_TUPLE_A_MAX
    ):
        quit(
            "Entered input is not a number or is not from range! Program exit!"
        )

    INT_INPUT_ = int(STR_INPUT)
    print(
        "Elements from original list that are smaller than "
        + str(INT_INPUT_)
        + ": "
        + str([element for element in INT_TUPLE_A if element < INT_INPUT_])
    )
    quit("End of program!")


if __name__ == "__main__":
    exercise_main()
