"""Excercise 05

Excercise from website:
https://www.practicepython.org/exercise/2014/03/05/05-list-overlap.html
"""

from random import randint


def exercise_main() -> None:
    """Generates two random lists and returns a third list with common
    items between LIST_A and LIST_B
    """
    # Constants
    INT_A_UPPERLIMIT: int = randint(50, 100)
    INT_B_UPPERLIMIT: int = randint(50, 100)
    INT_A_LOWERLIMIT: int = 0
    INT_B_LOWERLIMIT: int = 0
    INT_A_INCREMENT: int = randint(1, 5)
    INT_B_INCREMENT: int = randint(1, 5)
    INT_LIST_A: list[int] = list(
        range(INT_A_LOWERLIMIT, INT_A_UPPERLIMIT, INT_A_INCREMENT)
    )
    INT_LIST_B: list[int] = list(
        range(INT_B_LOWERLIMIT, INT_B_UPPERLIMIT, INT_B_INCREMENT)
    )

    c: set[int] = set([item for item in INT_LIST_A if item in INT_LIST_B])

    print("First list is: " + str(INT_LIST_A))
    print("Second list is: " + str(INT_LIST_B))
    print("Intersection list is: " + str(c))


if __name__ == "__main__":
    exercise_main()
