"""Excercise 15

Excercise from https://www.practicepython.org/exercise/2014/05/21/15-reverse-word-order.html
"""

# Constants
STR_INPUT: str = input("Enter a phrase: ")


def reverse_string_order_v1(string_input: str):
    """V1 reverse string order with lists and split/reverse/join funcions

    Args:
        string_input (string): string phrase

    Returns:
        string: separated words
    """
    # split sentence into words
    separated_words: str = string_input.split(" ")
    # reverse order of separated words
    separated_words.reverse()
    return print(" ".join(separated_words))


def reverse_string_order_v2(string_input: str):
    """V2 reverse string order with strings and split() + join()
    Args:
        string_input (string): string phrase

    Returns:
        string: separated words
    """
    separated_words: str = " ".join(string_input.split()[::-1])
    return print(separated_words)


def exercise_main(string_input: str):
    """Main function that executes V1 and V2 funcions

    Args:
        string_input (string): string phrase
    """

    reverse_string_order_v1(string_input)
    reverse_string_order_v2(string_input)


if __name__ == "__main__":
    exercise_main(STR_INPUT)
