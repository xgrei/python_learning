""" Excercise 06

This is script implementing Excercise 06 from:
https://www.practicepython.org/exercise/2014/03/12/06-string-lists.html
"""


def exercise_main() -> None:
    """Prints if a string input is a palindrome or not"""
    # Constants
    STR_WORD: str = input("Enter a word: ")
    STR_REVERSE_WORD: str = STR_WORD[::-1]
    STR_OUTPUT_TEXT: str = (
        STR_WORD + " is a palindrome."
        if (STR_WORD == STR_REVERSE_WORD)
        else STR_WORD + " is not a palindrome."
    )

    print(STR_OUTPUT_TEXT)


if __name__ == "__main__":
    exercise_main()
