# Python_Learning

Python learning projects

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Name
python_learning

## Description
Project is dedicated for learning and code review of set of assignments from:
- [practicepython.org](https://www.practicepython.org)
- [hackerrank.com](https://www.hackerrank.com)

## Installation
It is mandatory to install python3 interpreter together with package installer and virtual enviroments with all its corresponding dependencies.
`apt install python3 python3-pip python3-venv`

## Usage
Each file is its assignment and to execute it you need to pass it to the python interpreter `python3 </path/to/file.py>`

## Support
There is no support. Use at your own risk.

## Roadmap
We will add new assignments as we see fit.

## Contributing
We do not plan to accept contributions.

## License
Use at your own risk. Copy it, tinker with it, rewrite it or use it - we don't care.